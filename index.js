var nem2Sdk = require("nem2-sdk");
var operators = require("rxjs/operators");
var Account = nem2Sdk.Account,
    Mosaic = nem2Sdk.Mosaic,
    MosaicId = nem2Sdk.MosaicId,
    Deadline = nem2Sdk.Deadline,
    NetworkType = nem2Sdk.NetworkType,
    TransferTransaction = nem2Sdk.TransferTransaction,
    TransactionHttp = nem2Sdk.TransactionHttp,
    PlainMessage = nem2Sdk.PlainMessage,
    XEM = nem2Sdk.XEM,
    AggregateTransaction = nem2Sdk.AggregateTransaction,
    LockFundsTransaction = nem2Sdk.LockFundsTransaction,
    UInt64 = nem2Sdk.UInt64,
    Listener = nem2Sdk.Listener,
    Address = nem2Sdk.Address,
    PublicAccount = nem2Sdk.PublicAccount,
    filter = operators.filter,
    mergeMap = operators.mergeMap;

// 01 - Set up
var nodeUrl = "http://139.180.212.240:3000";
var transactionHttp = new TransactionHttp(nodeUrl);
var listener = new Listener(nodeUrl);

// 01 - Prepare mosaic to transfer.
var mosaicsToTransfer = [
    new Mosaic(new MosaicId("df:khr"), UInt64.fromUint(10)),
    new Mosaic(new MosaicId("df:usd"), UInt64.fromUint(10))
];

var initiatorPrivateKey =
    "3108AA77833182E047B981CBFD2C12CE47E7D48366FDA00B7ECFF35F67485888";
var initiatorAccount = Account.createFromPrivateKey(
    initiatorPrivateKey,
    NetworkType.MIJIN_TEST
);

var multisigAccountPublicKey = "4F9F5EC8C8283AF680B6D3DE575000E34303F0F16A58A29C205DC8F4D3F13683";
var multisigAccount = PublicAccount.createFromPublicKey(
    multisigAccountPublicKey,
    NetworkType.MIJIN_TEST
);

var recipientAddress = Address.createFromRawAddress(
    "SB2P672Y2KWQML4MTTYASZVHQKRH2EYI5PX5O7K4"
);

// 02 - Create transfer transaction
var transferTransaction = TransferTransaction.create(
    Deadline.create(),
    recipientAddress,
    mosaicsToTransfer,
    PlainMessage.create("aggregrate bonded txn"),
    NetworkType.MIJIN_TEST
);

// 02 - Create aggregate transaction
var aggregateTransaction = AggregateTransaction.createBonded(
    Deadline.create(),
    [transferTransaction.toAggregate(multisigAccount)],
    NetworkType.MIJIN_TEST
);

var signedTransaction = initiatorAccount.sign(aggregateTransaction);

var lockFundsTransaction = LockFundsTransaction.create(
    Deadline.create(),
    new Mosaic(new MosaicId("prx:xpx"), UInt64.fromUint(10)), // <== Mosaic to deposit in LockFundsTransaction
    UInt64.fromUint(480),
    signedTransaction,
    NetworkType.MIJIN_TEST
);

var lockFundsTransactionSigned = initiatorAccount.sign(lockFundsTransaction);

listener.open().then(() => {
    // 04 - Announce lockFundsTransactionSigned
    transactionHttp
        .announce(lockFundsTransactionSigned)
        .subscribe(x => console.log(x), err => console.error(err));

    // 05 - Announce aggregatedBonded signed transaction after lockFundsTransaction is confirmed
    listener
        .confirmed(initiatorAccount.address)
        .pipe(
            filter(
                transaction =>
                transaction.transactionInfo !== undefined &&
                transaction.transactionInfo.hash === lockFundsTransactionSigned.hash
            ),
            mergeMap(ignored =>
                transactionHttp.announceAggregateBonded(signedTransaction)
            )
        )
        .subscribe(
            announcedAggregateBonded => console.log(announcedAggregateBonded),
            err => console.error(err)
        );

    // 06 - Listen to errors
    // 👇 The error Failure_Lock_Invalid_Mosaic_Amount is handle here
    listener.status(initiatorAccount.address).subscribe(error => {
        console.error(error);
    });
});